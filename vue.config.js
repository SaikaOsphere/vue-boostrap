module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
        ? '/~cs62160270/learn_bootstrap/'
        : '/'
}